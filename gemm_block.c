#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "gemm_block.h"

#define BLOCK_SIZE BLOCK * BLOCK * sizeof(float)

#ifdef ACCEL
#include "xhls_accel.h"
#include "libaxidma/libaxidma.h"
axidma_dev_t dma_dev;
XHls_accel accel;
bool ACCEL_SW;

float* a;
float* b;
float* c;
float* out;

void gemm_hw(float* A, float* B, float* C)
{
	XHls_accel_Start(&accel);

	memcpy(a, A, BLOCK_SIZE);
	memcpy(b, B, BLOCK_SIZE);
	memcpy(c, C, BLOCK_SIZE);

	int tx_chan = axidma_get_dma_tx(dma_dev)->data[0];
	int rx_chan = axidma_get_dma_rx(dma_dev)->data[0];

	axidma_oneway_transfer(dma_dev, tx_chan, (void*)a, BLOCK_SIZE,true);
	axidma_oneway_transfer(dma_dev, tx_chan, (void*)b, BLOCK_SIZE,true);
	axidma_oneway_transfer(dma_dev, tx_chan, (void*)c, BLOCK_SIZE,true);

	axidma_oneway_transfer(dma_dev, rx_chan, (void*)out, BLOCK_SIZE,true);

	memcpy(C, out, BLOCK_SIZE);
}


#endif


float** create_block_matrix(int M, int N, float* A)
{
    int rows = ceil((float)M / BLOCK);
    int cols = ceil((float)N / BLOCK);
    float** A_part = malloc(rows * cols * sizeof(float*));
    for (int i = 0; i < rows * cols; i++)
    {
        A_part[i] = malloc(BLOCK_SIZE);
        float* part = A_part[i];

        int col = i % cols;
        int row = i / cols;

        for (int j = 0; j < BLOCK; j++)
        {
            for (int k = 0; k < BLOCK; k++)
            {
                if (col * BLOCK + k >= N)
                {
                    part[j * BLOCK + k] = 0.0f;
                }
                else if (row * BLOCK + j >= M)
                {
                    part[j * BLOCK + k] = 0.0f;
                }
                else
                {
                    part[j * BLOCK + k] = A[col * BLOCK + k + (row * BLOCK + j) * N];
                } 
            }
        }
    }

    return A_part;
}

void gemm_ref(int M, int N, int K, float *A, float *B, float *C)
{
    int i, j, k;
    for (i = 0; i < M; ++i)
    {
        for (k = 0; k < K; ++k)
        {
            float A_PART = A[i * K + k];
            for (j = 0; j < N; ++j)
            {
                C[i * N + j] += A_PART * B[k * N + j];
            }
        }
    }
}

void release_block_matrix(int M, int N, float** A)
{
    int num_parts = ceil((float)M / BLOCK) * ceil((float)N / BLOCK);
    for (int i = 0; i < num_parts; i++)
    {
        free(A[i]);
    }
    free(A);
}

void gemm_block(int M, int N, int K, float** A, float** B, float** C)
{
    int lm = (int)ceil((float)M / BLOCK);
    int ln = (int)ceil((float)N / BLOCK);
    int lk = (int)ceil((float)K / BLOCK);

    for (int i = 0; i < lm; ++i)
    {
        for (int j = 0; j < ln; ++j)
        {
            for (int k = 0; k < lk; ++k)
            {
                int a = i * lk + k;
                int b = k * ln + j;
                int c = i * ln + j;
#ifdef ACCEL
                if (ACCEL_SW)
					gemm_ref(BLOCK, BLOCK, BLOCK, A[a], B[b], C[c]);
                else
                	gemm_hw(A[a], B[b], C[c]);
#else
                gemm_ref(BLOCK, BLOCK, BLOCK, A[a], B[b], C[c]);
#endif
            }
        }
    }
}

float* get_matrix_from_part(int M, int N, float** A)
{
    float* a = malloc(N * M * sizeof(float));
    conv_block_to_matrix(M, N, A, a);
    return a;
}

void conv_block_to_matrix(int M, int N, float** A, float* a)
{
    int cols = ceil((float)N / BLOCK);

    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            int cur_col = j / BLOCK;
            int cur_row = i / BLOCK;
            a[i * N + j] = A[cur_row * cols + cur_col][(i % BLOCK) * BLOCK + (j % BLOCK)];
        }
    }
}

void print_block_matrix(int M, int N, float** A)
{
    float* a = get_matrix_from_part(M, N, A);
    print_regular_matrix(M, N, a);
    free(a);
}

void print_regular_matrix(int M, int N, float* A)
{
    printf("\n");
    for (int i = 0; i < M; i++)
    {
        for (int j = 0; j < N; j++)
        {
            printf("%6.1f ", A[i * N + j]);
        }
        printf("\n");
    }
}

void gemm_partition(int TA, int TB, int M, int N, int K, float ALPHA,
          float *A, int lda,
          float *B, int ldb,
          float BETA,
          float *C, int ldc)
{
    printf("GEMM: M = %d, N = %d, K = %d\n", M, N, K);
    
    float** a = create_block_matrix(M, K, A);
    float** b = create_block_matrix(K, N, B);
    float** c = create_block_matrix(M, N, C);

    gemm_block(M, N, K, a, b, c);
    conv_block_to_matrix(M, N, c, C);

    release_block_matrix(M, K, a);
    release_block_matrix(K, N, b);
    release_block_matrix(M, N, c);
}
