/*
 * Copyright (c) 2012 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <stdio.h>
#include <unistd.h>
#include "xhls_accel.h"
#include "libaxidma/libaxidma.h"
#include "test.h"
#include "gemm_block.h"

extern int darknet_main(int argc, char** argv);

axidma_dev_t dma_dev;
XHls_accel accel;
float* a;
float* b;
float* c;
float* out;
bool ACCEL_SW = false;

int main(int argc, char** argv)
{
#ifdef ACCEL
	int ret = XHls_accel_Initialize(&accel, "HLS_accel");
	if (ret)
	{
		printf("ERROR: Failed to init accel: %d\n", ret);
		return 0;
	}

	dma_dev = axidma_init();
	printf("INIT accel\n");

	if (!dma_dev)
	{
		printf("ERROR: Failed to init dma\n");
		goto accel_release;
	}

	a = axidma_malloc(dma_dev, BLOCK_SIZE);
	b = axidma_malloc(dma_dev, BLOCK_SIZE);
	c = axidma_malloc(dma_dev, BLOCK_SIZE);
	out = axidma_malloc(dma_dev, BLOCK_SIZE);

	if (argc == 1)
	{
		test_main();
	}
	else
	{
		chdir("/home/root/darknet");
		if (strcmp(argv[1], "sw") == 0)
		{
			ACCEL_SW = true;
			darknet_main(argc-1, &(argv[1]));
		}
		else
			darknet_main(argc, argv);
	}

	axidma_free(dma_dev, a, BLOCK_SIZE);
	axidma_free(dma_dev, b, BLOCK_SIZE);
	axidma_free(dma_dev, c, BLOCK_SIZE);

    axidma_destroy(dma_dev);
accel_release:
	XHls_accel_Release(&accel);
    printf("RELEASE accel\n");
    return 0;
#else
    chdir("/home/root/darknet");
	darknet_main(argc, argv);
	return 0;
#endif
}
